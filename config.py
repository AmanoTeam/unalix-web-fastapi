port_str = "8000"

app_port = int(port_str)

app_host = "0.0.0.0"

app_title = "Unalix-web"

app_description = "source code: https://github.com/AmanoTeam/unalix-web-fastapi."

app_version = "2.0"

app_debug_mode = False

show_server_errors = False
